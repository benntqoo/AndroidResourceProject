# 目錄
Android
* [TabLayout](#TabLayout)
* [Material Design](#Material Design)
* [自定義view](#自定義view)

Python
* [爬蟲](#爬蟲)

# TabLayout


1. [Material Design 系列 Samples](https://github.com/pinguo-zhouwei/MaterialDesignSamples)<br>
DEMO:<br>
![Demo](https://github.com/pinguo-zhouwei/MaterialDesignSamples/raw/master/material_design_simples.gif)<br>


2. [实现自定义的Tab](https://github.com/imgod1/TestTabIconText)<br><br><br><br>
3. [實現TABLAYOU依據VIEWPAGER選取的頁面呈現不同的ICON](https://solinariwu.blogspot.tw/2016/12/android-tablayouviewpagericon.html)<br>
DEMO:<br>
![Demo](https://2.bp.blogspot.com/-HGRLufbLn1g/WGn10FxSWKI/AAAAAAAAC7M/CMvAx4I7KR8mDWjyrL2paOORDUr1dMJjwCLcB/s320/1212.gif) <br>


4. [TabLayout简单使用](https://github.com/zilianliuxue/AndroidStudy/blob/master/Material%20design/Material%20design%E4%B9%8BTabLayout%E7%9A%84%E4%BD%BF%E7%94%A8.md)<br>
DEMO:<br>
![DEMO](https://camo.githubusercontent.com/4f1f51bbcc219a054da04ac65e2cb04fe1d56986/687474703a2f2f696d672e626c6f672e6373646e2e6e65742f3230313630393232313335313331343936)

5. [TabLayout实现底部自定义Tab布局](http://tneciv.xyz/android/2015/10/22/TabLayout/)<br>
DEMO:<br>
![DEMO](http://i.imgur.com/goeTYhB.gif)<br>

6. [[SnapEvent]Android 5.0 的Toolbar+Tab+ViewPager](http://mis101bird.js.org/snapeventwo/)<br>
DEMO:<br>
![DEMO](http://mis101bird.js.org/images/snapevent2/03.jpg)<br>


# Material Design
1. [Android Material Design Pattern Tutorial](https://github.com/goofyz/android-material-design-tutorial/tree/part7_tablayout) <br>
Part 1: Navigation Drawer<br>
Part 2: Floating Action Button<br>
Part 3: Snackbar and CoordinatorLayout<br>
Part 4: RecyclerView<br>
Part 5: AppBarLayout<br>
Part 6: TextInputLayout<br>
Part 7: TabLayout<br>


# 自定義view
1. [如何理解MeasureSpec](http://zheteng.me/android/2015/03/10/Android-understanding-MeasureSpec/)
2. [MeasureSpec介绍及使用详解](http://www.cnblogs.com/slider/archive/2011/11/28/2266538.html)
3. [自定义View](http://mp.weixin.qq.com/s?__biz=MzI1MTA3Mzk4Mg==&mid=2651020086&idx=1&sn=5e94a6ab5ac8a9a4d406399dadddfe29&chksm=f20f7fe7c578f6f1218c66d09eb49134748726874456749b82b069878b0a8dac71c865740f19&mpshare=1&scene=1&srcid=0107GTNkAL0GPcRkFdTWDo36#rd)
<br>
# 爬蟲
[huatian-funny](https://github.com/LiuRoy/huatian-funny)<br>
用决策树科学地分析自己的择偶观